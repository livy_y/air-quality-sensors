# air-quality-sensors

A sensor box made with esp32 to measure the air quality

## Getting started

You can run the project via platformIO. If you use a different board then you can change the settings in **platformio.ini**.

## features

- [x] measure CO
- [x] measure CO2
- [x] measure temperature and humidity
- [x] measure P2.5 and P10 (these are dust particles)
- [x] measure pressure
- [x] measure gas concentration
- [x] RT-clock
- [ ] Show sensor status with button and led

## Photos
<img title="Sensor Box 1" src="/images/box1.jpg" width="200">
<img title="Sensor Box 2" src="/images/box2.jpg" width="200">

## Authors and acknowledgment
This is a project for school. We work in a group of 5 people, Charlotte, Mathijs, Axel, Liv, and Mirte. Mathijs has made the sensor box design. Charlotte, Axel, Mirte have made reports about experiments. And lastly I, Liv have written the codee for the sensor box.

## License
This project is under the MIT-licence.
