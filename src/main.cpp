#include <Wire.h>
#include <SPI.h>  // communication protocol (leterlijke kunst)
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"  // red square sensor
#include "FS.h"  // sd
#include "SD.h"  // sd
#include "SPI.h"  // sd communication
#include "RTClib.h"  // to show the time
#include "SSD1306Wire.h"
#include <U8g2lib.h>
#include <SDS011.h>  // black box sensor
#include <MHZ.h>  // CO2 sensor
#include <DallasTemperature.h>  // temp sensor


// INIT VARIABLES
const int SDS_TX = 1;
const int SDS_RX = 3;
const int BUTTON_PIN = 15;
const int CO_SENSOR_PIN = 33;
const int CO2_SENSOR_PWM_PIN = 4;
const int CO2_SENSOR_A1_PIN = 27;
const int TEMP_SENSOR_PIN = 32;

const bool CO2_PREHEATING = false;

float p10, p25;
bool screen = true;
int delay_time = 150;  // time to wait in seconds at the end of all sensor measurements (recommended: 120)
int CO2_sensor_wait_time = 60;  // amount of seconds to wait for a response of the CO2 sensor (recommended: 130)
String file_name = "/data.txt";  // name of the file to write data to


// INIT CLASSES
Adafruit_BME680 bme;
RTC_DS3231 rtc;
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, -1);
SDS011 my_sds;
HardwareSerial port(2);
MHZ CO2_sensor(CO2_SENSOR_PWM_PIN, MHZ14A);
OneWire temp_sensor_onewire(TEMP_SENSOR_PIN);
DallasTemperature temp_sensor(&temp_sensor_onewire);


void writeFile(fs::FS &fs, const char *path, const char *message) {
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if (!file) {
        Serial.println("Failed to open file for writing");
        return;
    }
    if (file.print(message)) {
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
    file.close();
}


bool appendFile(fs::FS &fs, const char *path, String message) {
    File file = fs.open(path, FILE_APPEND);
    if (!file) {
        Serial.println("Failed to open file for appending");
        return false;
    }
    if (file.print(message)) {
        message.replace("\n", "");
        message.replace("\t", "   ");
        Serial.println("<" + String(message) + ">: " + "message appended");
    } else {
        Serial.println("Append failed");
    }
    file.close();
    return true;
}


void check_screen_switch() {
    if (digitalRead(BUTTON_PIN)) {
        screen = true;
    } else {
        if (screen) {
            u8g2.clearBuffer();
            u8g2.sendBuffer();
        }
        screen = false;
    }
}


void setup() {
    // INIT SERIAL
    Serial.begin(115200);
    while (!Serial);

    // REAL TIME CLOCK
    if (!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        while (1);
    }
    rtc.adjust(DateTime(__DATE__, __TIME__));

    // INIT SD CARD
    if(!SD.begin(5)){
        Serial.println("Card Mount Failed");
        Serial.flush();
        while (1) delay(10);
    }

    // SWITCH
    pinMode(BUTTON_PIN, INPUT_PULLUP);

    // OLED DISPLAY
    u8g2.begin();
    u8g2.setFont(u8g2_font_6x13_tf);

    // INIT SDS SENSOR (FINE DUST)
    my_sds.begin(&port);

    // CO SENSOR
    pinMode(CO_SENSOR_PIN, INPUT);

    // CO2 SENSOR
    pinMode(CO2_SENSOR_PWM_PIN, INPUT);
    CO2_sensor.setDebug(true);
    pinMode(CO2_SENSOR_A1_PIN, INPUT);

    if (CO2_sensor.isPreHeating() and CO2_PREHEATING == true) {
        Serial.println("----- PREHEATING CO2 SENSOR -----");
        while (CO2_sensor.isPreHeating()) {
            Serial.println("Preheating <" + String(millis() / 1000) + "/" + String(180) + ">");
            delay(5000);
        }
        Serial.println("----- DONE PREHEATING -----");
    }

    // TEMP SENSOR
    temp_sensor.begin();

    // INIT SENSORS
    Serial.println(F("BME680 async test"));

    if (!bme.begin()) {
        Serial.println(F("Could not find a valid BME680 sensor, check wiring!"));
        while (1);
    }

    // Set up oversampling and filter initialization
    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
    bme.setGasHeater(320, 150); // 320*C for 150 ms

    String headers = "millis \t time \t P2.5 (μg/m3) \t P10 (μg/m3) \t CO resistance \t CO2 concentration (ppm)\t \t temp from temp sensor (*C) \t temp from air quality sensor (*C) \t pressure (hPa) \t humidity (%) \t gas (kOhms)";
    appendFile(SD, file_name.c_str(), headers);
    delay(3000);
}


void loop() {
    // for things that only need to run sometimes
    if (millis() % 500) {
        check_screen_switch();
    }


    if (screen) {
        u8g2.clearBuffer();
    }


    DateTime now = rtc.now();
    String text = String(millis());
    text += "\t";

    //TIME
    text += String(now.hour());
    text += ":";
    text += String(now.minute());
    text += ":";
    text += String(now.second());
    text += "\t";

    // BME680 BEGIN MEASUREMENT
    unsigned long endTime = bme.beginReading();
    if (endTime == 0) {
        Serial.println(F("Failed to begin reading the BME680 sensor"));
    }

    // SDS011 SENSOR (BLACK BOX)
    if (!my_sds.read(&p25, &p10)) {
        (screen) ? u8g2.drawStr(5, 64, ("P2.5 = " + String(int(p25)) + ", P10 = " + String(int(p10))).c_str()) : 0;
        text += String(p25);
        text += "\t";

        text += String(p10);
        text += "\t";
    }

    // CO SENSOR
    text += String(analogRead(CO_SENSOR_PIN));
    text += "\t";

    // CO2 SENSOR ANALOG 1
    float CO2_out = analogRead(CO2_SENSOR_A1_PIN) * 3.3 / 4095;
    int gas_concentration = int((CO2_out-0) * (5000/2));
    Serial.println(String(gas_concentration));

    // CO2 SENSOR PWM
    int ppm_pwm = CO2_sensor.readCO2PWM(CO2_sensor_wait_time);
    text += String(ppm_pwm);
    text += "\t";

    // TEMP SENSOR
    temp_sensor.requestTemperatures();
    text += String(temp_sensor.getTempCByIndex(0));
    text += "\t";


    // BME680 READ MEASUREMENT
    if (!bme.endReading()) {
        Serial.println(F("Failed to complete reading :("));
        return;
    }

    (screen) ? u8g2.drawStr(5, 10, ("temp = " + String(bme.temperature) + "*C").c_str()) : false;
    text += String(bme.temperature);
    text += "\t";

    (screen) ? u8g2.drawStr(5, 24, ("p = " + String(bme.pressure) + "hPa").c_str()) : false;
    text += String(bme.pressure / 100.0);
    text += "\t";

    (screen) ? u8g2.drawStr(5, 38, ("hum = " + String(bme.humidity) + "%").c_str()) : false;
    text += String(bme.humidity);
    text += "\t";

    (screen) ? u8g2.drawStr(5, 52, ("gas = " + String(bme.gas_resistance) + "KOhms").c_str()) : false;
    text += String(bme.gas_resistance / 1000.0);
    text += "\n";

    bool file_State = appendFile(SD, file_name.c_str(), text);
    if (!file_State) {
        file_name = "/" + String(random(1111, 8888)) + ".txt";
        writeFile(SD, file_name.c_str(), String("\n").c_str());
    }
    if (screen) {
        u8g2.sendBuffer();
    }

    delay(delay_time * 1000);
}